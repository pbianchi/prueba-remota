<?php

	include_once "iSuperficie.php";

abstract class Figura implements iSuperficie{

	protected $base;
	protected $altura;
	protected $diametro;
	protected $tipoFigura;


	public function __construct(){


			$this->base = null;
			$this->altura = null;
			$this->diametro = null;
			$this->tipoFigura = null;

	}

	public function __toString()
	{
		$lbl = "";
		$lbl .= "Tipo de Figura => ".$this->tipoFigura."<br/>";
		$lbl .= "Base => ".$this->base."<br/>";
		$lbl .= "Altura => ".$this->altura."<br/>";
		$lbl .= "Diametro => ".$this->diametro."<br/>";
		$lbl .= "Superficie => ".$this->getSuperficie()."<br/>";

		return( $lbl );

	}

	public function setBase( $base = null )
	{
		$this->base = $base;
	}

	public function getBase( )
	{
		return( $this->base );
	}

	public function setAltura( $altura = null )
	{
		$this->altura = $altura;
	}

	public function getAltura( )
	{
		return( $this->altura );
	}

	public function setDiametro( $diametro = null )
	{
		$this->diametro = $diametro;
	}

	public function getDiametro( )
	{
		return( $this->diametro );
	}

	public function setTipoFigura( $tipoFigura = null )
	{
		$this->tipoFigura = $tipoFigura;
	}

	public function getTipoFigura( )
	{
		return( $this->tipoFigura );
	}

}

class Cuadrado extends Figura{

	public function __construct(){

		parent::__construct();
		$this->tipoFigura = "CUADRADO";
	}

	public function getSuperficie(){
		
		if( ! is_null( $this->getBase() ) && ! is_null( $this->getAltura() ) )
		{
			return( $this->getBase() * $this->getAltura() );
		}
		else
		{
			return null;
		}
	}

}


class Circulo extends Figura{

	public function __construct(){

		parent::__construct();
		$this->tipoFigura = "CIRCULO";
	}

	public function getSuperficie(){
		
		if( ! is_null( $this->getDiametro() ) )
		{
			$radio = $this->getDiametro() / 2 ;
			return( 3.14 * pow( $radio , 2 ) );
		}
		else
		{
			return null;
		}
	}

}

class Triangulo extends Figura{

	public function __construct(){

		parent::__construct();
		$this->tipoFigura = "TRIANGULO";
	}

	public function getSuperficie(){
		
		if( ! is_null( $this->getBase() ) && ! is_null( $this->getAltura() ) )
		{
			return( $this->getBase() * $this->getAltura() / 2 );
		}
		else
		{
			return null;
		}
	}

}

?>