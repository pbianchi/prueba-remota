function showError( mensaje , callback )
{
	//header
	$("#modal-alerta-lblheader").html( "Error" );
	
	//message
	$("#modal-alerta-lblmessage").html( mensaje );
	
	$( "#modal-alerta-btnclose-x" ).unbind( "click" );
	if( callback )
	{
		$("#modal-alerta-btnclose-x").bind( "click", callback );
	}
	
	$( "#modal-alerta-btnclose" ).unbind( "click" );
	if( callback )
	{
		$("#modal-alerta-btnclose").bind( "click", callback );
	}
	
	
	$('#modal-alerta').modal({
		backdrop: 'static',  
		keyboard: false
	});
	

	
	
	
	
}

function showAviso( mensaje , callback )
{
	//header
	$("#modal-alerta-lblheader").html( "Aviso" );
	
	//message
	$("#modal-alerta-lblmessage").html( mensaje );
	
	$( "#modal-alerta-btnclose-x" ).unbind( "click" );
	if( callback )
	{
		$("#modal-alerta-btnclose-x").bind( "click", callback );
	}
	
	$( "#modal-alerta-btnclose" ).unbind( "click" );
	if( callback )
	{
		$("#modal-alerta-btnclose").bind( "click", callback );
	}
	
	
	$('#modal-alerta').modal({
		backdrop: 'static',  
		keyboard: false
	});
	

	
}

function showConfirm( mensaje, ok_callback, cancel_callback )
{
	
	//header
	$("#modal-confirm-lblheader").html( "Confirmacion" );
	
	//message
	$("#modal-confirm-lblmessage").html( mensaje );
	
	
	
	
	$( "#modal-confirm-btn-ok" ).unbind( "click" );
	if( ok_callback )
	{	
		$("#modal-confirm-btn-ok").bind( "click", ok_callback );
	}
	
	$( "#modal-confirm-btn-cancel" ).unbind( "click" );
	if( cancel_callback )
	{	
		$("#modal-confirm-btn-cancel").bind( "click", cancel_callback );
	}
	
	
	$('#modal-confirm').modal({
		backdrop: 'static',  
		keyboard: false
	});
	

	
}

function showIframe( iframeURL , callback )
{
	//header
	$("#modal-alerta-lblheader").html( "" );
	
	
	
	var iframe = document.createElement('iframe');
	
	iframe.src = iframeURL;
	iframe.height = "100%";
	iframe.width = "100%";
	iframe.frameborder = "0"; 
	iframe.style="overflow:hidden;height:100%;width:100%;border:0";
	
	//message
	$("#modal-alerta-lblmessage").html( iframe );
	
	$( "#modal-alerta-btnclose-x" ).unbind( "click" );
	if( callback )
	{
		$("#modal-alerta-btnclose-x").bind( "click", callback );
	}
	
	$( "#modal-alerta-btnclose" ).unbind( "click" );
	if( callback )
	{
		$("#modal-alerta-btnclose").bind( "click", callback );
	}
	
	
	$('#modal-alerta').modal('show');
	
	
	
	
}

function microtime(get_as_float) {
	  //  discuss at: http://phpjs.org/functions/microtime/
	  // original by: Paulo Freitas
	  //   example 1: timeStamp = microtime(true);
	  //   example 1: timeStamp > 1000000000 && timeStamp < 2000000000
	  //   returns 1: true

	  var now = new Date()
	    .getTime() / 1000;
	  var s = parseInt(now, 10);

	  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
	}




//UPLOAD


function getUpload( id , tipo_upload , elemento  )
{
	var url =  "/CMS/upload/inicializa/" + id + "/" + tipo_upload +"/" + elemento ;
	
	if( typeof( elemento ) !== undefined )
	{
		url += "/" + elemento ;
	}
	
	$.ajax({
			url: url ,
			type: "POST",
			dataType: "json",
			error:function( jqXHR ,  textStatus,  errorThrown ){ console.log(jqXHR ) ; console.log(textStatus); console.log( errorThrown );   alert('error');},
			success:function( response ){
				$("[name=upload-"+tipo_upload+"]").html( response );
		  	}
	});
}


function mostrar_upload( id , tipo_upload )
{

	showIframe( "/CMS/upload/subir/"+ id + "/" + tipo_upload , function(){
		
		refresh_upload( id , tipo_upload );
		
	});
}

function refresh_upload( id , tipo_upload )
{

	//CIERRO EL MODAL
	$('#modal-alerta').modal('hide');
	
	//TRAIGO LOS UPLOADS
	getUpload( id , tipo_upload );
}



function loadCombo( ajaxResponse , combo , conSeleccione )
{
	$("[name="+combo+"]").html('');
	
	if( conSeleccione )
	{
		$("[name="+combo+"]").append('<option value="0">--Seleccione--</option>');
	}
	
	if( ajaxResponse !== null )
	{
	
		//recorremos todas las filas del resultado del proceso que obtenemos en Json
		$.each( ajaxResponse.data , 
			function(i,item)
			{
				
				$("[name="+combo+"]").append('<option value="'+item[ajaxResponse.id]+'">'+item[ajaxResponse.value]+'</option>');
			}
		);
	}
}

function cleanCombo( combo )
{
	$("[name="+combo+"]").html('');
	$("[name="+combo+"]").append('<option value="0">--Seleccione--</option>');
}