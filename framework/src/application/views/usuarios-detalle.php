<!DOCTYPE html>
<html lang="en">

<head>
    <title>Detalle Usuario <?= $oUsuario->getUsuario() ?></title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    <!-- Bootstrap Core CSS -->
    <link href="<? print( base_url() ); ?>assets/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<? print( base_url() ); ?>assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<? print( base_url() ); ?>assets/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- CUSTOM CSS -->
    <style>
    
    .form-control-data{
	    border: 1px solid #5E5E5E;
	    margin-bottom: 0;
	    min-height: 34px;
	    padding-bottom: 7px;
	    padding-top: 7px;
	    padding-left: 7px;
    }
    
    </style>
    
</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detalle Usuario <?= $oUsuario->getUsuario() ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           <div class="row">
				<div class="col-md-12">
					<div class="panel panel-inverse">
						<div class="panel-body">
							 <form  class="form-horizontal" >
                                
                                 <input type="hidden" value="<?= $oUsuario->getCodigoUsuario() ?>" name="codigo-usuario">
                                 <div class="form-group">
                                    <label class="col-md-2 control-label">Usuario</label>
                                    <div class="col-md-10">
                                        <p class="form-control-data"><?= $oUsuario->getUsuario() ?></p>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label class="col-md-2 control-label">Clave</label>
                                    <div class="col-md-10">
                                        <p class="form-control-data"><?= $oUsuario->getClave() ?></p>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label class="col-md-2 control-label">Edad</label>
                                    <div class="col-md-2">
                                        <p class="form-control-data"><?= $oUsuario->getEdad() ?></p>
                                    </div>
                                </div>
                                
                                
                                       
                            	<div class="form-group">
                                    <label class="col-md-2 control-label">Pagos</label>    
                            		<div class="col-md-10">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>C&oacute;digo</th>
                                            <th>Importe</th>
                                            <th>Fecha</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?
                                    	foreach( $lPagos AS $oPago )
                                    	{
                                    ?>
                                        <tr>
                                            <td><? print( $oPago->getCodigoPago() )?></td>
                                            <td><? print( $oPago->getImporte() )?></td>
                                            <td><? print( $oPago->getFecha()->format("d/m/Y") )?></td>
                                            
                                        </tr>
                                        <?
                                    	}
                                    ?> 
                                    </tbody>
                                </table>
                                	</div>
                                </div>
                                
                                 
                           
                                <div class="form-group">
                                    
                                    <div class="col-md-12" style="text-align: right;">
                                        <button type="button" name="btn-aceptar" class="btn btn-success">Aceptar</button>
                                    </div>
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
         
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->




        
	<script>
		var base_url = "<? print( base_url() ); ?>";
	</script>
	
	<!-- jQuery -->
    <script src="<? print( base_url() ); ?>assets/components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<? print( base_url() ); ?>assets/components/bootstrap/dist/js/bootstrap.min.js"></script>

   

   
   	
	<script>

	
		
	$(document).ready(function() {


				
		$("[name=btn-aceptar]").on( "click" , function(  )
				{
					window.history.back();
				});


 		
 		

 		
 		    
 		 
	});


 	
 

 	</script>

</body>

</html>