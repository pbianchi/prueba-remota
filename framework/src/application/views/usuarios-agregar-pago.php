<!DOCTYPE html>
<html lang="en">

<head>
    <title>Agregar Pago Usuario <?= $oUsuario->getUsuario() ?></title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    <!-- Bootstrap Core CSS -->
    <link href="<? print( base_url() ); ?>assets/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<? print( base_url() ); ?>assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<? print( base_url() ); ?>assets/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- CUSTOM CSS -->
    <style>
    
    .form-control-data{
	    border: 1px solid #5E5E5E;
	    margin-bottom: 0;
	    min-height: 34px;
	    padding-bottom: 7px;
	    padding-top: 7px;
	    padding-left: 7px;
    }
    
    </style>
    
</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Agregar Pago Usuario <?= $oUsuario->getUsuario() ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           <div class="row">
				<div class="col-md-12">
					<div class="panel panel-inverse">
						<div class="panel-body">
							 <form  class="form-horizontal" name="frm-agregar-pago" >
                                
                                 <input type="hidden" value="<?= $oUsuario->getCodigoUsuario() ?>" name="codigo-usuario">
                                 <div class="form-group">
                                    <label class="col-md-2 control-label">Usuario</label>
                                    <div class="col-md-10">
                                        <p class="form-control-data"><?= $oUsuario->getUsuario() ?></p>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label class="col-md-2 control-label">Clave</label>
                                    <div class="col-md-10">
                                        <p class="form-control-data"><?= $oUsuario->getClave() ?></p>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label class="col-md-2 control-label">Edad</label>
                                    <div class="col-md-2">
                                        <p class="form-control-data"><?= $oUsuario->getEdad() ?></p>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Pagos</label>
                                    <div class="col-md-2">
                                        <select class="form-control" name="pago" >
                                            <option value="">--Seleccione--</option>
                                            <? foreach( $lPagos AS $oPago ) { ?>    
                                            <option value="<?= $oPago->getCodigoPago() ?>"><?= $oPago->getImporte()." - ".$oPago->getFecha()->format("d/m/Y") ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                 
                           
                                <div class="form-group">
                                    
                                    <div class="col-md-12" style="text-align: right;">
                                        <button type="button" name="btn-agregar-pago" class="btn btn-success">Agregar Pago</button>
                                        <button type="button" name="btn-volver" class="btn btn-danger">Volver</button>
                                    </div>
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
         
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->



<!-- Modal -->
<div class="modal fade" id="modal-alerta" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="modal-alerta-btnclose-x">&times;</button>
				<h4 class="modal-title" id="modal-alerta-lblheader"></h4>
			</div>
			<div class="modal-body" id="modal-alerta-lblmessage"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="modal-alerta-btnclose">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal Confirm -->
<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"  id="modal-confirm-lblheader"></h4>
      </div>
      <div class="modal-body">
       <p id="modal-confirm-lblmessage" ></p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" id="modal-confirm-btn-ok">Si</button>
        <button class="btn btn-primary" data-dismiss="modal" id="modal-confirm-btn-cancel">No</button>
      </div>
    </div>
  </div>
</div>
        
	<script>
		var base_url = "<? print( base_url() ); ?>";
	</script>
	
	<!-- jQuery -->
    <script src="<? print( base_url() ); ?>assets/components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<? print( base_url() ); ?>assets/components/bootstrap/dist/js/bootstrap.min.js"></script>

   
<!--  -->
    <script src="<? print( base_url() ); ?>assets/js/basics.js"></script>
   
   	
	<script>

	
		
	$(document).ready(function() {


				
		$("[name=btn-volver]").on( "click" , function(  )
				{
					window.history.back();
				});


 		
 		
        $("[name=btn-agregar-pago]").on( "click" , function(  )
				{
					$.ajax({
						  	url: "<?php echo base_url() ?>usuarios/agregar-pago/",
							type: "POST",
							data: { "data" : JSON.stringify( $("[name=frm-agregar-pago]").serializeArray() ) },
							dataType: "json",
							error:function(){alert("Error");},
							success:function( response ){
								if( response.error == 1 )
								{
								
									showError( response.data );
								}
								else
								{
									showAviso( "Se ha agregado el Pago al Usuario" , function(){ window.location = "<?php echo base_url() ?>usuarios/" } )
								}
							}
					});
				});
 		
 		    
 		 
	});


 	
 

 	</script>

</body>

</html>