<!DOCTYPE html>
<html lang="en">

<head>
    <title>Usuarios</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    <!-- Bootstrap Core CSS -->
    <link href="<? print( base_url() ); ?>assets/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<? print( base_url() ); ?>assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<? print( base_url() ); ?>assets/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- CUSTOM CSS -->
    <style>
    
    .form-control-data{
	    border: 1px solid #5E5E5E;
	    margin-bottom: 0;
	    min-height: 34px;
	    padding-bottom: 7px;
	    padding-top: 7px;
	    padding-left: 7px;
    }
    
    </style>
</head>

<body>

    <div id="wrapper">

        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Usuarios</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            	<div class="row">
                            		<div style="text-align:right; padding-bottom:30px" class="col-md-12">
                            			<button type="button" name="btn-nuevo-usuario" class="btn btn-primary">Nuevo Usuario</button>
                            		</div>
                            		
                            	</div>
                            	
                            
                            	<div class="row">
                            		<div class="col-sm-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>C&oacute;digo</th>
                                            <th>Usuario</th>
                                            <th>Edad</th>
                                            
                                             <th>&nbsp;</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?
                                    	foreach( $lUsuarios AS $oUsuario )
                                    	{
                                    ?>
                                        <tr>
                                            <td><? print( $oUsuario->getCodigoUsuario() )?></td>
                                            <td><? print( $oUsuario->getUsuario() )?></td>
                                            <td><? print( $oUsuario->getEdad() )?></td>
                                            <td class="left">
                                            	<div>
                                            		<a href="<? print( base_url("usuarios/detalle/".$oUsuario->getCodigoUsuario() ) ); ?>"><i class="glyphicon glyphicon glyphicon-zoom-in"></i></a>&nbsp;
                                            		<a href="<? print( base_url("usuarios/modifica/".$oUsuario->getCodigoUsuario() ) ); ?>"><i class="glyphicon glyphicon glyphicon-pencil"></i></a>
                                                    <a href="<? print( base_url("usuarios/agregar-pago/".$oUsuario->getCodigoUsuario() ) ); ?>">[Agregar Pago]</a>
                                            	</div>
                                            </td>
                                        </tr>
                                        <?
                                    	}
                                    ?> 
                                    </tbody>
                                </table>
                                	</div>
                                </div>
                 
                                
                            </div>
                            <!-- /.table-responsive -->
                            
                            
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
         
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

   
   
   	<script>
		var base_url = "<? print( base_url() ); ?>";
	</script>
	
	<!-- jQuery -->
    <script src="<? print( base_url() ); ?>assets/components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<? print( base_url() ); ?>assets/components/bootstrap/dist/js/bootstrap.min.js"></script>

   
   
   	
	<script>
	$(document).ready(function() {
		
 		$("[name=btn-nuevo-usuario]").on( "click" , function(  )
		{
			window.location = "<?php echo base_url( "usuarios/alta") ?>"
		});
	});


 	
 

 	</script>

</body>

</html>