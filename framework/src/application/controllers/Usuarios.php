<?
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(  )
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->listado();
	}
	
	
	
	public function listado()
	{
        
        
        $this->load->model('usuario_model');

        $data = array();

        try 
        {
            $data["lUsuarios"] =  $this->usuario_model->getUsuarios( new UsuarioFiltro() ) ;
        }
        catch ( Exception $ex )
        {
            $data["lUsuarios"] = array();
        }

        $this->load->view( "usuarios.php" , $data );


	}
	
	public function detalle( $codigoUsuario = -1 )
	{
		
		
		$data = array();
			$this->load->model('usuario_model');
        $this->load->model('pago_model');
			
			$data["oUsuario"] = new Usuario();
        $data["lPagos"] = array();
			
			try
			{
				$data["oUsuario"] =  $this->usuario_model->getUsuarioxCodigoUsuario( $codigoUsuario ) ;
			
				if( is_null( $data["oUsuario"] ) )
				{
					print( "Usuario Inexistente");
                    die;
				}
				
					$data["lPagos"] =  $this->pago_model->getPagosxUsuario( $data["oUsuario"] ) ;
			}
			catch ( Exception $ex )
			{
				print( $ex->getMessage() );
                die;
			}
			
			
			
				
			$this->load->view( "usuarios-detalle.php" , $data );
	}
	
	public function alta(  )
	{
	
		
		if( ! $this->input->is_ajax_request() )
		{
			$data = array();
			
			$this->load->view( "usuarios-alta.php" , $data );
		}
		else
		{
            $this->load->library('form_validation');
			$this->load->helper('json_helper');
			$lResponse = array( "error" => 0 , "data" => "" );
				
			
			try
			{
				$oData = from_serializeArray( $this->input->post( 'data', true ) );
					
					
				$this->form_validation->set_data( from_object_to_validate_array( $oData ) );
				
				$this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|min_length[5]');
                $this->form_validation->set_rules('clave', 'Clave', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('edad', 'Edad', 'trim|required|numeric');
				
				if ( $this->form_validation->run() == FALSE )
				{
					$lResponse = array( "error" => 1 , "data" => "" );
					foreach(  $this->form_validation->error_array() AS $error )
					{
						$lResponse["data"] .= $error."<br>";
					}
						
				}
	
				if( $lResponse["error"] == 0 )
				{
						
					$this->load->model('usuario_model');
						
					$oUsuario = new Usuario();
					
					
					
					$oUsuario->setUsuario( $oData->{"usuario"} );
					$oUsuario->setClave( $oData->{"clave"} );
					$oUsuario->setEdad( $oData->{"edad"} );
					
											
					$this->usuario_model->altaUsuario( $oUsuario );
						
					
					
				}
                
					
					
			}
			catch ( Exception $ex )
			{
				$lResponse = array( "error" => 1 , "data" => $ex->getMessage() );
			}
				
				
				
				
				
				
			print( json_encode( $lResponse ) );
			die;
	
		}
			
	
	}
	
	
	public function modifica( $codigoUsuario = -1  )
	{
	
		
		if( ! $this->input->is_ajax_request() )
		{
			$data = array();
			$this->load->model('usuario_model');
			
			$data["oUsuario"] = new Usuario();
			
			try
			{
				$data["oUsuario"] =  $this->usuario_model->getUsuarioxCodigoUsuario( $codigoUsuario ) ;
			
				if( is_null( $data["oUsuario"] ) )
				{
					print( "Usuario Inexistente");
                    die;
				}
				
					
			}
			catch ( Exception $ex )
			{
				print( $ex->getMessage() );
                die;
			}
			
			
			
				
			$this->load->view( "usuarios-modifica.php" , $data );
		}
		else
		{
			$this->load->library('form_validation');
	
			$this->load->helper('json_helper');
			$lResponse = array( "error" => 0 , "data" => "" );
	
			try
			{
				$oData = from_serializeArray( $this->input->post( 'data', true ) );
					
					
				$this->form_validation->set_data( from_object_to_validate_array( $oData ) );
				
				$this->form_validation->set_rules('codigo-usuario', 'Código Usuario', 'trim|required|numeric');
                $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|min_length[5]');
                $this->form_validation->set_rules('clave', 'Clave', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('edad', 'Edad', 'trim|required|numeric');
				
				if ( $this->form_validation->run() == FALSE )
				{
					$lResponse = array( "error" => 1 , "data" => "" );
					foreach(  $this->form_validation->error_array() AS $error )
					{
						$lResponse["data"] .= $error."<br>";
					}
						
				}
	
				if( $lResponse["error"] == 0 )
				{
						
					$this->load->model('usuario_model');
						
					$oUsuario = new Usuario();
					
					
					$oUsuario->setCodigoUsuario( $oData->{"codigo-usuario"} );
					$oUsuario->setUsuario( $oData->{"usuario"} );
					$oUsuario->setClave( $oData->{"clave"} );
					$oUsuario->setEdad( $oData->{"edad"} );
					
											
					$this->usuario_model->modificaUsuario( $oUsuario );
						
					
					
				}
                
					
					
			}
			catch ( Exception $ex )
			{
				$lResponse = array( "error" => 1 , "data" => $ex->getMessage() );
			}
				
	
	
	
	
	
			print( json_encode( $lResponse ) );
			die;
	
		}
			
	
	}
	
    
    public function agregar_pago( $codigoUsuario = -1  )
	{
	
		
		if( ! $this->input->is_ajax_request() )
		{
			$data = array();
			$this->load->model('usuario_model');
            $this->load->model('pago_model');
			
			$data["oUsuario"] = new Usuario();
			$data["lPagos"] = array();
			try
			{
				$data["oUsuario"] =  $this->usuario_model->getUsuarioxCodigoUsuario( $codigoUsuario ) ;
			
				if( is_null( $data["oUsuario"] ) )
				{
					print( "Usuario Inexistente");
                    die;
				}
				
                $data["lPagos"] =  $this->pago_model->getPagos( new PagoFiltro() ) ;
			
				
					
			}
			catch ( Exception $ex )
			{
				print( $ex->getMessage() );
                die;
			}
			
			
			
				
			$this->load->view( "usuarios-agregar-pago.php" , $data );
		}
		else
		{
			$this->load->library('form_validation');
	
			$this->load->helper('json_helper');
			$lResponse = array( "error" => 0 , "data" => "" );
	
			try
			{
				$oData = from_serializeArray( $this->input->post( 'data', true ) );
					
					
				$this->form_validation->set_data( from_object_to_validate_array( $oData ) );
				
				$this->form_validation->set_rules('codigo-usuario', 'Código Usuario', 'trim|required|numeric');
                $this->form_validation->set_rules('pago', 'Pago', 'trim|required|numeric');
				
				if ( $this->form_validation->run() == FALSE )
				{
					$lResponse = array( "error" => 1 , "data" => "" );
					foreach(  $this->form_validation->error_array() AS $error )
					{
						$lResponse["data"] .= $error."<br>";
					}
						
				}
	
				if( $lResponse["error"] == 0 )
				{
					
					$this->load->model('usuario_model');
						
					$oUsuario = new Usuario();
					$oUsuario->setCodigoUsuario( $oData->{"codigo-usuario"} );
					
                    
                    $oPago = new Pago();
                    $oPago->setCodigoPago( $oData->{"pago"} );
					
											
					$this->usuario_model->agregarPagoUsuario(  $oPago ,  $oUsuario );
						
					
					
				}
                
					
					
			}
			catch ( Exception $ex )
			{
				$lResponse = array( "error" => 1 , "data" => $ex->getMessage() );
			}
				
	
	
	
	
	
			print( json_encode( $lResponse ) );
			die;
	
		}
			
	
	}
	

}
