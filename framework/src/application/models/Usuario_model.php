<?php

require_once( ENTITIES_DIR  . "Usuario.php");

class Usuario_model extends CI_Model { 
   
    public function __construct() {
        
        parent::__construct();
    }
   
   
   
	private function __UsuarioToSQLWhere( UsuarioFiltro $oUsuario )
   	{
   		$lFiltro = array( "where" => array() , "values" => array() , "where_str" => "" );
   		
   		if( $oUsuario->getCodigoUsuario() != 0 )
   		{
   			array_push( $lFiltro["where"] , "Usuarios.codigousuario =  ? " );
   			array_push( $lFiltro["values"] , $oUsuario->getCodigoUsuario() );
   		}
   		
   		if( $oUsuario->getUsuario() != "" )
   		{
   			array_push( $lFiltro["where"] , "UPPER( Usuarios.usuario ) =  ? " );
   			array_push( $lFiltro["values"] , strtoupper( trim( $oUsuario->getUsuario() ) ) );
   		}
   	
   		if( $oUsuario->getClave() != "" )
   		{
   			array_push( $lFiltro["where"] , "Usuarios.passwclaveord =  ? " );
   			array_push( $lFiltro["values"] , trim( md5( $oUsuario->getClave() ) ) );
   		}
   		
        foreach( $lFiltro["where"] AS $condicion )
	   	{
	   		
	   		if( $lFiltro["where_str"] != "" )
	   		{
	   			$lFiltro["where_str"] .= " AND " ;
	   		}
	   		
	   		$lFiltro["where_str"] .= $condicion ;
	   	}
	   	
	   	if( trim( $lFiltro["where_str"] ) != "" )
	   	{
	   		$lFiltro["where_str"] = " WHERE ".$lFiltro["where_str"] ;
	   	}
   
   		return( $lFiltro ) ;
   }
   
   private function __UsuarioMapper( $record )
   {
   
   	    $oUsuario = new Usuario();
   
        if( array_key_exists( "codigousuario" , $record ) )
        {
            $oUsuario->setCodigoUsuario( $record["codigousuario"] );
        }
   
        if( array_key_exists( "usuario" , $record ) )
        {
            $oUsuario->setUsuario( $record["usuario"] ) ;
        }
       
        if( array_key_exists( "clave" , $record ) )
        {
            $oUsuario->setClave( $record["clave"] ) ;
        }
   
        if( array_key_exists( "edad" , $record ) )
        {
            $oUsuario->setEdad( $record["edad"] ) ;
        }
        
       return( $oUsuario ) ;
   }
      
   private function __UsuarioselectSQL()
   {
	   $strCommand = "";
	   
	   $strCommand .= " SELECT DISTINCT ";
	   $strCommand .= "		Usuarios.codigousuario,";
	   $strCommand .= "		Usuarios.usuario,";
	   $strCommand .= "		Usuarios.clave,";
	   $strCommand .= "		Usuarios.edad";
	   $strCommand .= " FROM ";
	   $strCommand .= "		Usuarios";
	   
	   return( $strCommand );
   }
   
   
    private function validarDatos( Usuario $oUsuario )
    {
        $msgError = "";
        
        if( trim( $oUsuario->getUsuario() ) == "" )
        {
            $msgError .= "Debe completar el Usuario<br/>";
        }
        else
        {
            $oUsuarioDB = $this->getUsuarioxUsuario( $oUsuario->getUsuario() );
            if( ! is_null( $oUsuarioDB ) && $oUsuarioDB->getCodigoUsuario() != $oUsuario->getCodigoUsuario() )
            {
                $msgError .= "Ya existe un usuario con el mismo Usuario registrado<br/>";
            }
        }
        
        if( trim( $oUsuario->getClave() ) == "" )
        {
            $msgError .= "Debe completar la Clave<br/>";
        }
        
        if( trim( $oUsuario->getEdad() ) == "" )
        {
            $msgError .= "Debe completar la Edad<br/>";
        }
        else if( ! is_numeric( $oUsuario->getEdad() ) )
        {
            $msgError .= "La Edad debe ser un número<br/>";
        }
        else if( $oUsuario->getEdad() < 18 )
        {
            $msgError .= "La Edad no puede ser menor a 18<br/>";
        }
        
        if( $msgError != "" )
        {
            throw new Exception( $msgError );
        }
    }
    
    
   
   public function altaUsuario( Usuario $oUsuario )
   {
   	 
        try
        {
            //VALIDO LOS DATOS
            $this->validarDatos( $oUsuario );

            $this->db->trans_begin();

            $data = array(
                    'usuario' => $oUsuario->getUsuario(),
                    'clave' => $oUsuario->getClave(),
                    'edad' => $oUsuario->getEdad()

            );

            $this->db->insert('Usuarios', $data );

            $db_error = $this->db->error();
            if(  $db_error["code"] != 0 )
            {
                throw new Exception( $db_error["message"] );
            }

            $this->db->trans_commit();

        }
        catch ( Exception $ex )
        {
            $this->db->trans_rollback();
            throw $ex;
        }
   	 
   	 
   }
   
   public function modificaUsuario( Usuario $oUsuario )
   {
   	 
   	try
   	{
        //VALIDO LOS DATOS
        $this->validarDatos( $oUsuario );
        
   		$this->db->trans_begin();
   		 
   		 
   	    $data = array(
                    'usuario' => $oUsuario->getUsuario(),
                    'clave' => $oUsuario->getClave(),
                    'edad' => $oUsuario->getEdad()

        );
   		
        $this->db->where('codigousuario', $oUsuario->getCodigoUsuario() );
   		$this->db->update('Usuarios', $data);
   		 
   		$db_error = $this->db->error();
   		if(  $db_error["code"] != 0 )
   		{
   			throw new Exception( $db_error["message"] );
   		}
   		
   		$this->db->trans_commit();
   
   	}
   	catch ( Exception $ex )
   	{
   		$this->db->trans_rollback();
   		throw $ex;
   	}
   	 
   	 
   }
    
    
    public function agregarPagoUsuario( Pago $oPago , Usuario $oUsuario )
   {
   	 
        try
        {
            //VALIDO QUE NO TENGA EL PAGO VINCULADO
            $this->load->model( "pago_model" );
            
            $oUsuarioDB = $this->getUsuarioxCodigoUsuario( $oUsuario->getCodigoUsuario() );
            $oPagoDB = $this->pago_model->getPagoxCodigoPago( $oPago->getCodigoPago() );
            $oPagoUsuario = $this->pago_model->getPagoxUsuario( $oPago , $oUsuario );
            
            
            if( is_null( $oUsuarioDB ) )
            {
                throw new Exception( "Usuario Inexistente");
            }
            
            if( is_null( $oPagoDB ) )
            {
                throw new Exception( "Pago Inexistente");
            }
            
            if( ! is_null( $oPagoUsuario ) )
            {
                throw new Exception( "El Usuario ya tiene el Pago Vinculado");
            }
            

            $this->db->trans_begin();

            $data = array(
                    'codigousuario' => $oUsuario->getCodigoUsuario(),
                    'codigopago' => $oPago->getCodigoPago()
            );

            $this->db->insert('UsuariosPagos', $data );

            $db_error = $this->db->error();
            if(  $db_error["code"] != 0 )
            {
                throw new Exception( $db_error["message"] );
            }

            $this->db->trans_commit();

        }
        catch ( Exception $ex )
        {
            $this->db->trans_rollback();
            throw $ex;
        }
   	 
   	 
   }
   
   
   public function getUsuario( UsuarioFiltro $oUsuarioFiltro )
   {
   		
   		$strCommand = "";
   		$lFilter = array();
   		$oUsuarioDevolver = new Usuario() ;
   
	   	try
	   	{
	   		$strCommand .= $this->__UsuarioselectSQL();
	   			
	   		$lFilter = $this->__UsuarioToSQLWhere( $oUsuarioFiltro ) ;
	   
	   		if( $lFilter["where_str"] != "" )
	   		{
	   			$strCommand .= $lFilter["where_str"];
	   		}
	   
	   		
	   		$query = $this->db->query( $strCommand , $lFilter["values"] );
	   		$db_error = $this->db->error();
	   		if(  $db_error["code"] != 0 )
	   		{
	   			throw new Exception( $db_error["message"] );
	   		}
	   		 
	   		if( $query->num_rows() == 1 )
	   		{
	   			$oUsuarioDevolver = $this->__UsuarioMapper( $query->row_array() );
	   		}
	   		else
	   		{
	   			$oUsuarioDevolver = null;
	   		}
	   		 
	   		
	   		return $oUsuarioDevolver;
	   	}
	   	catch( Exception $ex )
	   	{
	   		throw new Exception( $ex->getMessage() ) ;
	   
	   	}
   }
   
   
   public function getUsuarios( UsuarioFiltro $oUsuarioFiltro )
   {
   	 
        $strCommand = "";
        $lFilter = array();
        $lUsuariosDevolver = array() ;

        try
        {
            $strCommand .= $this->__UsuarioselectSQL();

            $lFilter = $this->__UsuarioToSQLWhere( $oUsuarioFiltro ) ;

            if( $lFilter["where_str"] != "" )
            {
                $strCommand .= $lFilter["where_str"];
            }


            $query = $this->db->query( $strCommand , $lFilter["values"] );
            $db_error = $this->db->error();
            if(  $db_error["code"] != 0 )
            {
                throw new Exception( $db_error["message"] );
            }

            if( $query->num_rows() >= 1 )
            {
                foreach ( $query->result_array() as $reg )
                {
                     $oUsuarioDevolver = new Usuario();
                    $oUsuarioDevolver = $this->__UsuarioMapper( $reg );

                    array_push( $lUsuariosDevolver , $oUsuarioDevolver );
                }
            }



            return $lUsuariosDevolver;
        }
        catch( Exception $ex )
        {
            throw new Exception( $ex->getMessage() ) ;

        }
   }
    
   public function getUsuarioxUsuario( $usuario = "" )
   {
   	 
        $oUsuarioDevolver = new Usuario() ;

        try
        {
            if( trim( $usuario ) == "" )
            {
                throw new Exception( "Debe ingresar el Usuario" );
            }
            
            $oUsuarioFiltro = new UsuarioFiltro();
            $oUsuarioFiltro->setUsuario( $usuario );

            $oUsuarioDevolver = $this->getUsuario( $oUsuarioFiltro );


            return $oUsuarioDevolver;
        }
        catch( Exception $ex )
        {
            throw new Exception( $ex->getMessage() ) ;

        }
   }
    
   public function getUsuarioxCodigoUsuario( $codigousuario = -1 )
   {
   	 
        $oUsuarioDevolver = new Usuario() ;

        try
        {
            $oUsuarioFiltro = new UsuarioFiltro();
            $oUsuarioFiltro->setCodigoUsuario( $codigousuario );

            $oUsuarioDevolver = $this->getUsuario( $oUsuarioFiltro );


            return $oUsuarioDevolver;
        }
        catch( Exception $ex )
        {
            throw new Exception( $ex->getMessage() ) ;

        }
   }
    
   
   
   
}