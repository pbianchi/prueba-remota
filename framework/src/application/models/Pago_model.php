<?php

require_once( ENTITIES_DIR  . "Pago.php");

class Pago_model extends CI_Model { 
   
    public function __construct() {
        
        parent::__construct();
    }
   
   
   
	private function __PagoToSQLWhere( PagoFiltro $oPago )
   	{
   		$lFiltro = array( "where" => array() , "values" => array() , "where_str" => "" );
   		
   		if( $oPago->getCodigoPago() != 0 )
   		{
   			array_push( $lFiltro["where"] , "Pagos.codigopago =  ? " );
   			array_push( $lFiltro["values"] , $oPago->getCodigoPago() );
   		}
   		
   		
        foreach( $lFiltro["where"] AS $condicion )
	   	{
	   		
	   		if( $lFiltro["where_str"] != "" )
	   		{
	   			$lFiltro["where_str"] .= " AND " ;
	   		}
	   		
	   		$lFiltro["where_str"] .= $condicion ;
	   	}
	   	
	   	if( trim( $lFiltro["where_str"] ) != "" )
	   	{
	   		$lFiltro["where_str"] = " WHERE ".$lFiltro["where_str"] ;
	   	}
   
   		return( $lFiltro ) ;
   }
   
   private function __PagoMapper( $record )
   {
   
   	    $oPago = new Pago();
   
        if( array_key_exists( "codigopago" , $record ) )
        {
            $oPago->setCodigoPago( $record["codigopago"] );
        }
   
        if( array_key_exists( "importe" , $record ) )
        {
            $oPago->setImporte( $record["importe"] ) ;
        }
       
        if( array_key_exists( "fecha" , $record ) )
        {
            $oPago->setFecha( new DateTime( $record["fecha"] ) ) ;
        }
        
       return( $oPago ) ;
   }
      
   private function __PagoselectSQL()
   {
	   $strCommand = "";
	   
	   $strCommand .= " SELECT DISTINCT ";
	   $strCommand .= "		Pagos.codigopago,";
	   $strCommand .= "		Pagos.importe,";
	   $strCommand .= "		Pagos.fecha";
	   $strCommand .= " FROM ";
	   $strCommand .= "		Pagos";
	   
	   return( $strCommand );
   }
    
    private function __PagoUsuarioselectSQL()
    {
	   $strCommand = "";
	   
	   $strCommand .= " SELECT DISTINCT ";
	   $strCommand .= "		Pagos.codigopago,";
	   $strCommand .= "		Pagos.importe,";
	   $strCommand .= "		Pagos.fecha";
	   $strCommand .= " FROM ";
	   $strCommand .= "		Pagos";
       $strCommand .= "     INNER JOIN UsuariosPagos ON ( UsuariosPagos.codigopago = Pagos.codigopago )";
        
	   
	   return( $strCommand );
   }
   
   
    private function validarDatos( Pago $oPago )
    {
        $msgError = "";
        
   
        
        if( trim( $oPago->getImporte() ) == "" )
        {
            $msgError .= "Debe completar el Importe<br/>";
        }
        else if( ! is_numeric( $oPago->getImporte() ) )
        {
            $msgError .= "El Importe debe ser un número<br/>";
        }
        else if( $oPago->getImporte() < 1 )
        {
            $msgError .= "El Importe debe ser mayor a cero<br/>";
        }
        
        $hoy = new DateTime();
        $dif = $hoy->diff( $oPago->getFecha() );
        
        if( $dif->format( '%R%a' ) < 0 )
        {
           $msgError .= "La fecha no puede ser anterior al día de hoy.<br/>"; 
        }
        
        
        if( $msgError != "" )
        {
            throw new Exception( $msgError );
        }
    }
    
    
   
   public function altaPago( Pago $oPago )
   {
   	 
        try
        {
            //VALIDO LOS DATOS
            $this->validarDatos( $oPago );

            $this->db->trans_begin();

            $data = array(
                    'importe' => $oPago->getImporte(),
                    'fecha' => $oPago->getFecha()->format( "Y-m-d H:I:S")

            );

            $this->db->insert('Pagos', $data );

            $db_error = $this->db->error();
            if(  $db_error["code"] != 0 )
            {
                throw new Exception( $db_error["message"] );
            }

            $this->db->trans_commit();

        }
        catch ( Exception $ex )
        {
            $this->db->trans_rollback();
            throw $ex;
        }
   	 
   	 
   }
   
   public function modificaPago( Pago $oPago )
   {
   	 
   	try
   	{
        //VALIDO LOS DATOS
        $this->validarDatos( $oPago );
        
   		$this->db->trans_begin();
   		 
   		 
   	    $data = array(
                    'importe' => $oPago->getImporte(),
                    'fecha' => $oPago->getFecha()->format( "Y-m-d H:I:S")
        );
   		
        $this->db->where('codigopago', $oPago->getCodigoPago() );
   		$this->db->update('Pagos', $data);
   		 
   		$db_error = $this->db->error();
   		if(  $db_error["code"] != 0 )
   		{
   			throw new Exception( $db_error["message"] );
   		}
   		
   		$this->db->trans_commit();
   
   	}
   	catch ( Exception $ex )
   	{
   		$this->db->trans_rollback();
   		throw $ex;
   	}
   	 
   	 
   }
   
   public function getPago( PagoFiltro $oPagoFiltro )
   {
   		
   		$strCommand = "";
   		$lFilter = array();
   		$oPagoDevolver = new Pago() ;
   
	   	try
	   	{
	   		$strCommand .= $this->__PagoselectSQL();
	   			
	   		$lFilter = $this->__PagoToSQLWhere( $oPagoFiltro ) ;
	   
	   		if( $lFilter["where_str"] != "" )
	   		{
	   			$strCommand .= $lFilter["where_str"];
	   		}
	   
	   		
	   		$query = $this->db->query( $strCommand , $lFilter["values"] );
	   		$db_error = $this->db->error();
	   		if(  $db_error["code"] != 0 )
	   		{
	   			throw new Exception( $db_error["message"] );
	   		}
	   		 
	   		if( $query->num_rows() == 1 )
	   		{
	   			$oPagoDevolver = $this->__PagoMapper( $query->row_array() );
	   		}
	   		else
	   		{
	   			$oPagoDevolver = null;
	   		}
	   		 
	   		
	   		return $oPagoDevolver;
	   	}
	   	catch( Exception $ex )
	   	{
	   		throw new Exception( $ex->getMessage() ) ;
	   
	   	}
   }
   
   
   public function getPagos( PagoFiltro $oPagoFiltro )
   {
   	 
        $strCommand = "";
        $lFilter = array();
        $lPagosDevolver = array() ;

        try
        {
            $strCommand .= $this->__PagoselectSQL();

            $lFilter = $this->__PagoToSQLWhere( $oPagoFiltro ) ;

            if( $lFilter["where_str"] != "" )
            {
                $strCommand .= $lFilter["where_str"];
            }


            $query = $this->db->query( $strCommand , $lFilter["values"] );
            $db_error = $this->db->error();
            if(  $db_error["code"] != 0 )
            {
                throw new Exception( $db_error["message"] );
            }

            if( $query->num_rows() >= 1 )
            {
                foreach ( $query->result_array() as $reg )
                {
                     $oPagoDevolver = new Pago();
                    $oPagoDevolver = $this->__PagoMapper( $reg );

                    array_push( $lPagosDevolver , $oPagoDevolver );
                }
            }



            return $lPagosDevolver;
        }
        catch( Exception $ex )
        {
            throw new Exception( $ex->getMessage() ) ;

        }
   }
    
    public function getPagosxUsuario( Usuario $oUsuario )
   {
   	 
        $strCommand = "";
        $lFilter = array();
        $lPagosDevolver = array() ;

        try
        {
            $strCommand .= $this->__PagoUsuarioselectSQL();

            $strCommand .= " WHERE UsuariosPagos.codigousuario = ? " ;
            $strCommand .= " ORDER BY Pagos.fecha DESC ";
            


            $query = $this->db->query( $strCommand , array( $oUsuario->getCodigoUsuario() ) );
            $db_error = $this->db->error();
            if(  $db_error["code"] != 0 )
            {
                throw new Exception( $db_error["message"] );
            }

            if( $query->num_rows() >= 1 )
            {
                foreach ( $query->result_array() as $reg )
                {
                     $oPagoDevolver = new Pago();
                    $oPagoDevolver = $this->__PagoMapper( $reg );

                    array_push( $lPagosDevolver , $oPagoDevolver );
                }
            }



            return $lPagosDevolver;
        }
        catch( Exception $ex )
        {
            throw new Exception( $ex->getMessage() ) ;

        }
   }
    
    public function getPagoxUsuario( Pago $oPago ,  Usuario $oUsuario )
   {
   	 
        $strCommand = "";
        $lFilter = array();
        $oPagoDevolver = new Pago();

        try
        {
            $strCommand .= $this->__PagoUsuarioselectSQL();

            $strCommand .= " WHERE UsuariosPagos.codigousuario = ? " ;
            $strCommand .= " AND Pagos.codigopago = ? " ;
            


            $query = $this->db->query( $strCommand , array( $oUsuario->getCodigoUsuario() , $oPago->getCodigoPago() ) );
            $db_error = $this->db->error();
            if(  $db_error["code"] != 0 )
            {
                throw new Exception( $db_error["message"] );
            }

           if( $query->num_rows() == 1 )
	   		{
	   			$oPagoDevolver = $this->__PagoMapper( $query->row_array() );
	   		}
	   		else
	   		{
	   			$oPagoDevolver = null;
	   		}


            return $oPagoDevolver;
        }
        catch( Exception $ex )
        {
            throw new Exception( $ex->getMessage() ) ;

        }
   }
    
    
   public function getPagoxCodigoPago( $codigopago = -1 )
   {
   	 
        $oPagoDevolver = new Pago() ;

        try
        {
            $oPagoFiltro = new PagoFiltro();
            $oPagoFiltro->setCodigoPago( $codigopago );

            $oPagoDevolver = $this->getPago( $oPagoFiltro );


            return $oPagoDevolver;
        }
        catch( Exception $ex )
        {
            throw new Exception( $ex->getMessage() ) ;

        }
   }
    
   
   
   
}