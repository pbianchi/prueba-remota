<?

class Pago
{
	protected $codigoPago;
	protected $importe;
    protected $fecha;
	
	
	function __construct( $codigoPago = 0  , $importe = 0, DateTime $fecha = null )
	{
		$this->codigoPago = $codigoPago;
		$this->importe = $importe;
		$this->fecha = $fecha;
        if( is_null( $fecha ) )
        {
            $this->fecha = new DateTime();
        }
		
	}
	
	
	public function getCodigoPago()
	{
		return $this->codigoPago;
	}

	public function setCodigoPago( $codigoPago = 0 )
	{
		$this->codigoPago = $codigoPago;
	}
	
	public function getImporte()
	{
		return $this->importe;
	}
	
	public function setImporte( $importe = 0 )
	{
		$this->importe = $importe;
	}
	
	public function getFecha()
	{
		return $this->fecha;
	}
	
	public function setFecha( DateTime $fecha  )
	{
		$this->fecha = $fecha;
	}
	
	
}


class PagoFiltro extends Pago
{
   
    
	function __construct()
	{
		parent::__construct();
        
      
	}
    
 
	
}



?>