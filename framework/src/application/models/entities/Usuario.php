<?
require_once( ENTITIES_DIR  . "Pago.php");
class Usuario
{
	protected $codigoUsuario;
	protected $usuario;
	protected $clave;
	protected $edad;
    protected $lFavoritos;
    protected $lPagos;
	
	
	function __construct( $codigoUsuario = 0  , $usuario = "", $clave = "", $edad = 0)
	{
		$this->codigoUsuario = $codigoUsuario;
		$this->usuario = $usuario;
		$this->clave = $clave;
		$this->edad = 0;
        
		$this->lFavoritos = array();
		$this->lPagos = array();
		
	}
	
	
	public function getCodigoUsuario()
	{
		return $this->codigoUsuario;
	}

	public function setCodigoUsuario( $codigoUsuario = 0 )
	{
		$this->codigoUsuario = $codigoUsuario;
	}
	
	public function getUsuario()
	{
		return $this->usuario;
	}
	
	public function setUsuario( $usuario = "" )
	{
		$this->usuario = $usuario;
	}
	
	public function getClave()
	{
		return $this->clave;
	}
	
	public function setClave( $clave = "" )
	{
		$this->clave = $clave;
	}
	
	public function getEdad()
	{
		return $this->edad;
	}

	public function setEdad( $edad = 0 )
	{
		$this->edad = $edad;
	}
	
	public function getFavoritos()
	{
		return $this->lFavoritos;
	}
	
	public function setEmail( $lFavoritos = array() )
	{
		$this->lFavoritos = $lFavoritos;
	}
	
	public function getPagos()
	{
		return $this->lPagos;
	}
	
	public function setPagos( $lPagos = array() )
	{
		return $this->lPagos = $lPagos ;
	}
}


class UsuarioFiltro extends Usuario
{
	function __construct()
	{
		parent::__construct();
	}
	
}



?>