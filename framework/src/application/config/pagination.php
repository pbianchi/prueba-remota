<?php

$config['num_links'] = 2;

$config['full_tag_open'] = '<ul class="pagination">';
$config['full_tag_close'] = '</ul>';



$config['first_link'] = 'Primera';
$config['first_tag_open'] = '<li class="paginate_button">';
$config['first_tag_close'] = '</li>';

$config['prev_link'] = '&lt;';
$config['prev_tag_open'] = '<li class="paginate_button">';
$config['prev_tag_close'] = '</li>';


$config['next_link'] = '&gt;';
$config['next_tag_open'] = '<li class="paginate_button">';
$config['next_tag_close'] = '</li>';

$config['last_link'] = 'Ultima';
$config['last_tag_open'] = '<li class="paginate_button">';
$config['last_tag_close'] = '</li>';


$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
$config['cur_tag_close'] = '</a></li>';



$config['num_tag_open'] = '<li class="paginate_button">';
$config['num_tag_close'] = '</li>';

$config['use_page_numbers'] = TRUE;