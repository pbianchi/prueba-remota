<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('from_serializeArray'))
{
	function from_serializeArray( $jsonData )
	{
		$lData = json_decode( $jsonData );
		
		$oData = new stdClass();
		
		foreach( $lData as $key => $value )
		{
			if( property_exists( $oData , $value->name ) )
			{
				$oData->{$value->name} .= ";".$value->value;
			}
			else
			{
				$oData->{$value->name} = $value->value;
			}	
		}
		
		return( $oData );
	}
}

if ( ! function_exists('from_object_to_validate_array'))
{
	function from_object_to_validate_array( $jsonObject )
	{
		$lData = array();

		

		foreach( $jsonObject as $name => $value )
		{
			$lData[$name] = $value;
		}

		return( $lData );
	}
}


?>