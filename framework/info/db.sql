CREATE TABLE IF NOT EXISTS `Usuarios` (
  `codigousuario` INT(4) NOT NULL AUTO_INCREMENT,
  `usuario` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(255) NOT NULL,
  `edad` INT(4) NOT NULL,
  PRIMARY KEY (`codigousuario`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `Pagos` (
  `codigopago` INT(4) NOT NULL AUTO_INCREMENT,
  `importe` DECIMAL(9,2) NOT NULL,
  `fecha` DATETIME NOT NULL,
  PRIMARY KEY (`codigopago`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `UsuariosPagos` (
  `codigousuario` INT(4) NOT NULL,
  `codigopago` INT(4) NOT NULL,
  PRIMARY KEY (`codigousuario`, `codigopago`),
  INDEX `fk_UsuariosPagos_Pagos1_idx` (`codigopago` ASC),
  CONSTRAINT `fk_UsuariosPagos_Usuarios1`
    FOREIGN KEY (`codigousuario`)
    REFERENCES `Usuarios` (`codigousuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UsuariosPagos_Pagos1`
    FOREIGN KEY (`codigopago`)
    REFERENCES `Pagos` (`codigopago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `Favoritos` (
  `codigousuario` INT(4) NOT NULL,
  `codigousuario_favorito` INT(4) NOT NULL,
  PRIMARY KEY (`codigousuario`, `codigousuario_favorito`),
  INDEX `fk_Favoritos_Usuarios1_idx` (`codigousuario_favorito` ASC),
  CONSTRAINT `fk_Favoritos_Usuarios`
    FOREIGN KEY (`codigousuario`)
    REFERENCES `Usuarios` (`codigousuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Favoritos_Usuarios1`
    FOREIGN KEY (`codigousuario_favorito`)
    REFERENCES `Usuarios` (`codigousuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


INSERT INTO Pagos ( importe, fecha ) VALUES ( 100.20 , '2018-01-01 15:15:40' ),( 68.62 , '2018-01-03 10:28:40' );